<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    protected $task;

    public function  __construct(Task $task)
    {
        $this->task = $task;
    }
    public function index()
    {
        $tasks = $this->task->latest('id')->search()->paginate(10);
        return view( 'tasks.index', compact('tasks'));
    }
    public function store(CreateTaskRequest $request)
    {
        $this->task->create($request->all());
        return redirect()->route('tasks.index');
    }
    public function edit($id)
    {
        $task = $this->task->findOrFail($id);
        return view('tasks.edit',compact("task"));
    }

    public function update(CreateTaskRequest $request, $id)
    {
        $article =$this->task->findOrFail($id);
        $article->update($request->all());
        return redirect()->route('tasks.index');
    }

    public function destroy($id)
    {
        $article =$this->task->findOrFail($id);
        $article->delete();
        return redirect()->route('tasks.index');
    }

    public function create()
    {
        return view('tasks.create');
    }
}

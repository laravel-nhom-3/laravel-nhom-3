<?php

namespace App\Models;

use App\Models\Task;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = 'tasks';

    protected $fillable = ['name', 'content'];

    public function scopeSearch($query)
    {
        if($key = request()->key){
            $query = $query->where('name', 'like', '%' . $key . '%');
        }
        return $query;
    }
}

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class = "row justify-content-conter">
            <form action="" class="form-inline">
                <div class="form-group row">
                    <div class="col-md-6">
                        <input class="form-control" name="key" placeholder="Name">
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-secondary">Search</button>
                    </div>
                    {{-- <div class="col-md-3">
                        <button class="btn btn-secondary"><link href="../views/tasks/create.blade.php">Add</button>
                    </div> --}}
                </div>
                
            </form>
            <table class="table table-bordered mt-3">
                <thead class="table-dark">
                    <tr style="text-align: center">
                      <th  scope="col">ID</th>
                      <th scope="col">Name</th>
                      <th scope="col">Content</th>
                      <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <th scope="row">{{$task->id}}</th>
                            <th scope="row">{{$task->name}}</th>
                            <th scope="row" >{{$task->content}}</th>
                            <th>
                                <form method="POST" action="{{ route('tasks.destroy', $task->id) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-dark">Delete</button>
                                    <a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-warning">Edit</a>
                                </form>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$tasks->links()}}
        </div>
    </div>
@endsection
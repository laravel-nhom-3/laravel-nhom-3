<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListTastTest extends TestCase
{
    public function getListTastRoute()
    {
        return route('tasks.index');
    }
    /** @test */
    public function user_can_get_all_task()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getListTastRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertSee($task->name);
    }
}
